var config = require("../config");
var FCM = require('fcm-push');
var fcm = new FCM(config.FCM_KEY);

module.exports = {
    FCM_notification: function (fcm_data) {
            var message = {};
            message.to = fcm_data.token;
            message.collapse_key = "FCM Testig";
            var notification = {};
            notification.title = "FCM Testig";
            notification.body = "Welcome to FCM Testig";
            message.notification = notification;
            var data = {};
            data.nick = "FCM Testig";
            data.room = "Test";
            message.data = data;
            console.log(message);
            fcm.send(message, function (err, response) { 
                if (err) {
                    console.log("Something has gone wrong!");
                } else {
                    console.log("Successfully sent with response: ", response);
                }
            });

    }
};