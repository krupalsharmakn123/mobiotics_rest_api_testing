var bCrypt = require('bcrypt-nodejs');
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var BASE_API_URL = "";
var version = "1.0"; // version code

/* congig */
var configuration = require("../../config");

/* utils*/

/* handler */
var ProductHandler = require("../../handlers/product_handler");

/* Generates hash using bCrypt*/
var createHash = function (password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

/* Authentication function*/
var isAuthenticatedAccessToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers.authorization;
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, configuration.TOKEN_SECRET, function (err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other
                req.user = decoded;
                next();
            }
        });
    } else {
        return res.status(403).send({
            statuscode: 203,
            msgkey: "api.access.token.failed",
            v: version
        });
    }
}

module.exports = function (passport) {

    /*API to accress root*/
    router.get(BASE_API_URL + '/', function (req, res) {
        var reponseJson = {
            statuscode: 200,
            msgkey: "login.first.to.access.api",
            v: version
        };
        res.json(reponseJson);
    });

    /* sign up user exist*/
    router.get(BASE_API_URL + '/_signupfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "auth.signup.exist",
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Login*/
    router.post(BASE_API_URL + '/login', function (req, res, next) {
        passport.authenticate('login', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "login failure",
                    v: version
                };
                res.json(reponseJson);

            } else {
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "login success",
                    v: version,
                    data: user
                };
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for GET Home Page */
    router.get(BASE_API_URL + '/home', function (req, res) {
        if (req.user.username) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "login.success",
                v: version,
                data: req.user
            };
        }
        res.json(reponseJson);
    });

    /* API for login failure*/
    router.get(BASE_API_URL + '/_loginfailure', function (req, res) {
        var reponseJson = {
            statuscode: 203,
            msgkey: "login.failure",
            registered: false,
            v: version
        };
        res.json(reponseJson);
    });

    /* API for Handle Logout */
    router.get(BASE_API_URL + '/logout', function (req, res) {
        req.session.destroy(function (err) {
            var reponseJson = {
                statuscode: 200,
                msgkey: "logout.success",
                v: version
            };
            res.json(reponseJson);
        });

    });

    /* API for Handle Registration POST */
    router.post('/signup', function (req, res, next) {
        passport.authenticate('signup', function (err, user, info) {
            if (err) {
                var reponseJson = {
                    statuscode: 203,
                    msgkey: "auth.signup.exist",
                    v: version
                };
                res.json(reponseJson);
            } else if (info) {
                //console.log("User info" + user);
                //  console.log("Info: ", info);
            } else {
                var registered = false;
                var reponseJson = {
                    statuscode: 200,
                    msgkey: "auth.signup.success",
                    username: user.username,
                    v: version
                };
                req.logout();
                res.json(reponseJson);
            }
        })(req, res, next);
    });

    /* API for signup success page */
    router.get(BASE_API_URL + '/_signupsuccess', function (req, res) {
        var registered = false;
        if (req.user.isActive === "1") {
            registered = true;
        }
        var reponseJson = {
            statuscode: 200,
            msgkey: "auth.signup.success",
            registered: registered,
            username: req.user.username,
            v: version
        };
        req.logout();
        res.json(reponseJson);
    });

    router.post(BASE_API_URL + '/add_product', function (req, res) {
        var data = req.body;
        ProductHandler.add_product(data).then(function(response) {
        //ProductHandler.add_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/get_product', function (req, res) {
        var data = req.query;
        ProductHandler.get_product(data).then(function(response) {
        //ProductHandler.get_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.get(BASE_API_URL + '/list_products', function (req, res) {
       ProductHandler.list_products().then(function(response) {
       //ProductHandler.list_products(function (response) {
            //response.version = version;
            res.json(response);
        });
    });

    router.put(BASE_API_URL + '/update_product', function (req, res) {
        var data = req.body;
        ProductHandler.update_product(data).then(function(response) {
        //ProductHandler.update_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.post(BASE_API_URL + '/update_product_status', function (req, res) {
        var data = req.body;
        ProductHandler.update_product_status(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    router.delete(BASE_API_URL + '/delete_product', function (req, res) {
        var data = req.body;
        ProductHandler.delete_product(data).then(function(response) {
        //ProductHandler.delete_product(data, function (response) {
            response.version = version;
            res.json(response);
        });
    });

    return router;
};
