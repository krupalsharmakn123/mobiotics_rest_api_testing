var mongoose = require('mongoose');
module.exports = mongoose.model('Otp', {
    mobile : String,
    otp : String,
    expireAt: {
        type: Date,
        default: Date.now,
        index: { expires: '5m' },
    },
});
