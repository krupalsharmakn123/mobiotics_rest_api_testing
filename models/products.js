var mongoose = require('mongoose');
module.exports = mongoose.model('products', {
    product_id: String,
    product_name: String,
    product_des: String,
    isActive: { type:Boolean, default:true}
});
