var mongoose = require('mongoose');
module.exports = mongoose.model('user', {
    user_id : String,
    user_role : String,
    password : String,
    username : String,
    email : String,
    mobile : Number,
    passtoken : String,
    resetpasswordtoken : String,
    fcm_token: String,
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: Date,
    email_verified : {type: Boolean, default: false},
    is_active : {type: Boolean, default: true}
});
