var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var OTP = require('../models/otp');
var bCrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var configurations = require("../config");


module.exports = function (passport) {
    passport.use('otp_login', new LocalStrategy({
        usernameField: 'mobile',
        passwordField: 'otp',
        passReqToCallback: true
    }, function (req, mobile, otp, done) {
        var data = req.body;
        var date = new Date().toUTCString();
        OTP.findOne({
            'mobile': data.mobile,
            'otp': data.otp
        }, function (err, user) {
            console.log(user);
            if (err) {
                return done({ error: err });
            }
            if (!user) {
                console.log('User Not Found with mobile number ' + data.mobile);
                return done(null, false, { 'message': 'User Not found.' });
            }

            if (user) {
                is_profile_created = true;
                user.expiresAt = null;
                user.jwt_token = "";
                var jwt_object = {};
                jwt_object.mobile = user.mobile;
                jwt_object.user_id = user.user_id;
                var jwt_token = jwt.sign({
                    data: jwt_object
                    //exp: Math.floor(Date.now() / 1000) + (2 * 60 * 60),
                }, configurations.TOKEN_SECRET);
                user.jwt_token = jwt_token;

                console.log("getUsers data fetch successful" + is_profile_created);
                User.findOne({
                    'mobile': user.mobile
                }, function (err, user_details) {
                    console.log(jwt_object);
                    if (user_details == null) {
                        return res.json({
                            success: false,
                            message: 'user_details does not exist'
                        });
                    } else {
                        return done(null, {
                            jwt_token: user.jwt_token,
                            user_id: user_details.user_id,
                            user_role: user_details.user_role,
                            username: user_details.username,
                            mobile: user_details.mobile,
                            is_profile_created: is_profile_created
                        });
                    }
                });
            } else {

                is_profile_created = false;
                console.log('Error  in getUsers : ' + is_profile_created);
                return done(null, {
                    jwt_token: user.jwt_token,
                    user_id: user_details.user_id,
                    user_role: user_details.user_role,
                    username: user_details.username,
                    mobile: user_details.mobile,
                    is_profile_created: is_profile_created
                });
            }
        });

    })
    );
    // Utility Function
    var isValidPassword = function (user, password) {
        console.log(password.toString());
        console.log(user.password);
        return bCrypt.compareSync(password.toString(), user.password);
    }

};