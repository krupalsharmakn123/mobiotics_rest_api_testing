var Product = require("../models/products");

module.exports = {

    list_products: async function () {
        return new Promise(function (resolve, reject) {
            Product.find({}, function (err, products) {
                try {
                    resolve({ "msg": "List of products", "statuscode": 200, "data": products })
                    //return Promise.resolve({ "msg": "List of products", "statuscode": 200, "data": products })
                    console.log("UUUUUUUU")
                } catch (error) {
                    error.message;
                }
            });
        })
    },

    add_product: async function (data) {
        return new Promise(function (resolve, reject) {
            let addproduct = new Product();
            addproduct.product_id = data.product_id;
            addproduct.product_name = data.product_name;
            addproduct.product_des = data.product_des;
            addproduct.save(function (err, result) {
                try {
                    resolve({ "msg": "List of products", "statuscode": 200, "data": products })
                    //return Promise.resolve({ "msg": "List of products", "statuscode": 200, "data": products })
                    console.log("UUUUUUUU")
                } catch (error) {
                    error.message;
                }
            });

            //}
            // });
        })
        //process.nextTick(productdata);
    },

    get_product: function (data) {
        return new Promise(function (resolve, reject) {
            Product.findOne({
                "product_id": data.product_id
            }, function (err, product) {
                if (err) {
                    return reject(err)
                } else if (product == null) {
                    return reject(err)
                }
                return resolve({ "msg": "product details", "statuscode": 200 })
            });
        })
        //process.nextTick(productData);
    },

    update_product: function (data) {
        return new Promise(function (resolve, reject) {
            if (data.product_id == undefined || data.product_id == '') {
                return reject(err)
            } else {
                Product.findOne({
                    "product_id": data.product_id,
                }, function (err, productdata) {
                    var newdata = productdata;
                    if (err) {
                        return reject(err)
                    } else if (productdata == null) {
                        return reject(err)
                    } else {
                        if (data.product_name !== undefined && data.product_name !== "") {
                            newdata.product_name = data.product_name;
                        }
                        if (data.product_des !== undefined && data.product_des !== "") {
                            newdata.product_des = data.product_des;
                        }
                        newdata.save(function (err, productupdated) {
                            return resolve({ "msg": "Product updated Successfully", "statuscode": 200, "data": productupdated })
                        });
                    }
                });
            }
        })
        //process.nextTick(update_product_data);
    },

    delete_product: function (data) {
        return new Promise(function (resolve, reject) {
            if (data.product_id == undefined || data.product_id == '') {
                return reject(err)
            } else {
                Product.deleteOne({
                    "product_id": data.product_id
                }, function (err, product) {
                    if (err) {
                        return reject(err)
                    } else if (product == null) {
                        return reject(err)
                    }
                    return resolve({ "msg": "product deleted successfully", "statuscode": 200 })
                });
            }
        })
        //process.nextTick(delete_product_data);
    },

    update_product_status: function (data, callback) {
        update_product_status_data = function () {
            if (data.product_id == undefined || data.product_id == '') {
                callback({
                    msgkey: "Product not found",
                    statuscode: 404,
                });
                return;
            }
            Product.findOneAndUpdate({
                "product_id": data.product_id
            }, {
                $set: {
                    "isActive": data.isActive
                }
            }, {
                upsert: false,
                new: true
            }, function (err, result) {
                if (err) {
                    callback({
                        statuscode: 500,
                        msgkey: "database error"
                    });
                } else if (result == null) {
                    callback({
                        statuscode: 404,
                        msgkey: "Product not found "
                    });
                } else {
                    callback({
                        statuscode: 200,
                        msgkey: "Status updated successfully "
                    });
                }
            });
        }
        process.nextTick(update_product_status_data);
    }
}
