const product_handler = require('./product_handler');


test('List Product', () => {
    let output = {
        "msg": "List of products",
        "statuscode": 200,
        "version": "1.0"
    }
    product_handler.list_products().then(function(response) {
    //product_handler.list_products(async function (response) {
    expect(response.msg).toEqual(output.msg);
    expect(response).not.toBeNull();
    })
});

test('Add Product', () => {
    let input = {
        product_id: "p100",
        product_name: "samsung1",
        product_des: "Good1"
    }
    let output = {
        "msg": "Product added successfully",
        "statuscode": 201,
        "version": "1.0"
    }
    product_handler.add_product().then(function(response) {
    //product_handler.add_product(async function (response) {
        expect(product_handler.add_product(input))
        expect(response.msg).toEqual(output.msg);
        expect(response).not.toBeNull();
    })
});

test('Get Product', () => {
    let input = {
        product_id: "p100",
        product_name: "samsung1",
        product_des: "Good1"
    }
    let output = {
        "msg": "Product details",
        "statuscode": 200,
        "version": "1.0"
    }
    product_handler.get_product().then(function(response) {
    //product_handler.add_product(async function (response) {
        expect(product_handler.get_product(input))
        expect(response.statuscode).toEqual(output.statuscode);
        expect(response).not.toBeNull();
    })
});

test('Update Product', () => {
    let input = {
        product_id: "p100",
        product_name: "samsung1",
        product_des: "Good1"
    }
    let output = {
        "msg": "Product updated Successfully",
        "statuscode": 200,
        "version": "1.0"
    }
    product_handler.update_product().then(function(response) {
    //product_handler.add_product(async function (response) {
        expect(product_handler.update_product(input))
        expect(response.statuscode).toEqual(output.statuscode);
        expect(response).not.toBeNull();
    })
});

test('Delete Product', () => {
    let input = {
        product_id: "p100",
        product_name: "samsung1",
        product_des: "Good1"
    }
    let output = {
        "msg": "product deleted successfully",
        "statuscode": 200,
        "version": "1.0"
    }
    product_handler.delete_product().then(function(response) {
    //product_handler.add_product(async function (response) {
        expect(product_handler.delete_product(input))
        expect(response.statuscode).toEqual(output.statuscode);
        expect(response).not.toBeNull();
    })
});

